package br.com.itau;

public class Main {
    public static void main(String[] args) {

        Impressora impressora = new Impressora();
        Cliente cliente;
        ProdutoInvestimento produtoInvestimento;

        cliente = impressora.exibeCliente();

        produtoInvestimento = impressora.exibeInvestimento();

        produtoInvestimento.projetaInvestimento();

        impressora.imprimeInvestimento(cliente, produtoInvestimento);

    }
}
