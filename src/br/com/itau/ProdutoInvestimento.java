package br.com.itau;

public class ProdutoInvestimento {

    private double valorInvestimento;
    private double taxa;
    private int periodo;
    private double valorAcumulado;

    public ProdutoInvestimento() {
        this.taxa = 0.7;
    }

    public void setValorInvestimento(double valorInvestimento) {
        this.valorInvestimento = valorInvestimento;
    }

    public double getValorInvestimento() {
        return valorInvestimento;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public int getPeriodo() {
        return periodo;
    }

    public double getValorAcumulado() {
        return valorAcumulado;
    }

    public void projetaInvestimento() {
        this.valorAcumulado = this.valorInvestimento;

//        for (int i = 1; i <= this.periodo; i++) {
//            valorAcumulado += valorAcumulado * taxa;
//        }

        int i = 1;
        while (i <= this.periodo) {
            valorAcumulado += (valorAcumulado * taxa) / 100;
            i++;
        }

    }

}
