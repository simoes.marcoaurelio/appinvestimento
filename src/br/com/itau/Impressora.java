package br.com.itau;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Impressora {

    Scanner scanner = new Scanner(System.in);
    Cliente cliente = new Cliente();
    ProdutoInvestimento produtoInvestimento = new ProdutoInvestimento();
    DecimalFormat campoDecimal = new DecimalFormat("0.00");

    public Cliente exibeCliente() {

        System.out.println("Digite seu nome: ");
        cliente.setNome(scanner.nextLine());

        System.out.print("Olá ");
        System.out.print(cliente.getNome());

        return this.cliente;
    }

    public ProdutoInvestimento exibeInvestimento() {
        System.out.println(", qual valor deseja investir?");
        produtoInvestimento.setValorInvestimento(scanner.nextDouble());

        System.out.println("Por quantos meses você deseja manter o valor investido?");
        produtoInvestimento.setPeriodo(scanner.nextInt());

        return this.produtoInvestimento;
    }

    public void imprimeInvestimento(Cliente cliente, ProdutoInvestimento produtoInvestimento) {
        System.out.println(cliente.getNome() + ", se você investir R$" +
                campoDecimal.format(produtoInvestimento.getValorInvestimento()) + " reais por " +
                produtoInvestimento.getPeriodo() + " meses, terá um valor acumulado de R$" +
                campoDecimal.format(produtoInvestimento.getValorAcumulado()) + " reais ao final do período!");
    }

}
